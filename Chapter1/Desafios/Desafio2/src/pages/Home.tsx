import React, { useState } from 'react';
import { Alert, StyleSheet, View } from 'react-native';

import { Header } from '../components/Header';
import { TasksList } from '../components/TasksList';
import { Task } from '../components/TaskItem';
import { TodoInput } from '../components/TodoInput';

export function Home() {
  const [tasks, setTasks] = useState<Task[]>([]);

  function handleAddTask(newTaskTitle: string) {
    if (tasks.find(task => task.title == newTaskTitle) === undefined){
      const data = {
        id: new Date().getTime(),
        title: newTaskTitle,
        done: false
      }
      setTasks(oldTasks => [...oldTasks, data])
    }else{
      Alert.alert("Task já cadastrada", 
          "Você não pode cadastrar uma task com o mesmo nome", 
          [{text: "Ok"}])
    }
    
  }

  function handleToggleTaskDone(id: number) {
    setTasks(oldTask => oldTask.filter(
      task => task.id == id ? task.done = true : task
    ));
  }

  function RemoveTask(id: number) {
    setTasks(oldTask => oldTask.filter(
          task => task.id !== id
        ));
  }

  function handleRemoveTask(id: number) {
    Alert.alert("Remover item", 
      "Tem certeza que você deseja remover esse item?", 
      [{text: "Não"},
      {text: "Sim",
        onPress: () => RemoveTask(id)
      }])
  }

  return (
    <View style={styles.container}>
      <Header tasksCounter={tasks.length} />

      <TodoInput addTask={handleAddTask} />

      <TasksList 
        tasks={tasks}
        toggleTaskDone={handleToggleTaskDone}
        removeTask={handleRemoveTask}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EBEBEB'
  }
})