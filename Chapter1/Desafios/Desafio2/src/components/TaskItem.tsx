import React, { useRef, useEffect, useState }from 'react';
import { Image, TouchableOpacity, View, StyleSheet, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import { ItemWrapper } from './ItemWrapper';

import trashIcon from '../assets/icons/trash/trash.png'
import editIcon from '../assets/icons/edit/edit.png'

export interface Task {
    id: number;
    title: string;
    done: boolean;
}

export interface TasksItemProps {
    item: Task;
    index: number;
    toggleTaskDone: (id: number) => void;
    removeTask: (id: number) => void;
}

export function TaskItem({ item, index, toggleTaskDone, removeTask }: TasksItemProps) {

    const textItemRef = useRef<TextInput>(null)

    const [newTitle, setNewTitle] = useState(item.title);
    const [editing, setEditing] = useState(false);

    useEffect(() => {
        editing ? textItemRef.current?.focus() : textItemRef.current?.blur()
    }, [editing])

    function handleEditing(){
        setEditing(true);
      }
    
    function handleCancelEditing(){
        setEditing(false);
        setNewTitle(item.title)
     }

    function handleSubmitEditing(item:Task){
        item.title=newTitle
        setEditing(false);
     }

    return (
        <ItemWrapper index={index}>
            <View>
                <TouchableOpacity
                    testID={`button-${index}`}
                    activeOpacity={0.7}
                    style={styles.taskButton}
                    onPress={() => toggleTaskDone(item.id)}
                >
                    <View 
                        testID={`marker-${index}`}
                        style={ item.done == true ? styles.taskMarkerDone : styles.taskMarker }
                    >
                        { item.done && (
                            <Icon 
                                name="check"
                                size={12}
                                color="#FFF"
                            />
                        )}
                    </View>

                    <TextInput
                        style={ item.done == true ? styles.taskTextDone : styles.taskText }
                        value={newTitle}
                        editable={editing}
                        onChangeText={text => setNewTitle(text)}
                        onSubmitEditing={ () => handleSubmitEditing(item) }
                        ref={textItemRef}
                    />
                </TouchableOpacity>
            </View>
            {editing ?
                    <TouchableOpacity
                        testID={`cancel-${index}`}
                        style={{ paddingHorizontal: 24 }}
                        onPress={() => handleCancelEditing()}
                    >
                        <Icon 
                                name="x"
                                size={12}
                                color="#FFF"
                        />
                    </TouchableOpacity>
            :
                <>
                    <TouchableOpacity
                        testID={`edit-${index}`}
                        style={{ paddingHorizontal: 24 }}
                        onPress={() => handleEditing()}
                    >
                        <Image source={editIcon} />
                    </TouchableOpacity>

                    <TouchableOpacity
                        testID={`trash-${index}`}
                        style={{ paddingHorizontal: 24 }}
                        onPress={() => removeTask(item.id)}
                    >
                        <Image source={trashIcon} />
                    </TouchableOpacity>
                </>
            }
            
        </ItemWrapper>
    )
}
const styles = StyleSheet.create({
    taskButton: {
        flex: 1,
        paddingHorizontal: 24,
        paddingVertical: 15,
        marginBottom: 4,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center'
    },
    taskMarker: {
        height: 16,
        width: 16,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#B2B2B2',
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    taskText: {
        color: '#666',
        fontFamily: 'Inter-Medium'
    },
    taskMarkerDone: {
        height: 16,
        width: 16,
        borderRadius: 4,
        backgroundColor: '#1DB863',
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    taskTextDone: {
        color: '#1DB863',
        textDecorationLine: 'line-through',
        fontFamily: 'Inter-Medium'
    }
})