import React from 'react';
import { FlatList } from 'react-native';

import { TaskItem, Task } from './TaskItem';



interface TasksListProps {
  tasks: Task[];
  toggleTaskDone: (id: number) => void;
  removeTask: (id: number) => void;
}

export function TasksList({ tasks, toggleTaskDone, removeTask}: TasksListProps) {
  return (
    <FlatList
      data={tasks}
      keyExtractor={item => String(item.id)}
      contentContainerStyle={{ paddingBottom: 24 }}
      showsVerticalScrollIndicator={false}
      renderItem={({ item, index }) => {
        return (
          <TaskItem
            index={index}
            item={item}
            toggleTaskDone={toggleTaskDone}
            removeTask={removeTask}
          />
        )
      }}
      style={{
        marginTop: 32
      }}
    />
  )
}

